package cn.rock;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 网关服务
 */
@SpringCloudApplication
@EnableFeignClients("cn.rock.api")
@EnableZuulProxy
public class GgpServiceApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder ( GgpServiceApplication.class ).web ( true ).run ( args );
    }
}
