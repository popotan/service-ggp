package cn.rock.filter;

import com.netflix.zuul.ZuulFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 发送错误调用
 *
 * @author rock
 * @date 2017/7/7
 */
@Component
public class ZuulErrorFilter extends ZuulFilter {

    private static Logger logger = LoggerFactory.getLogger ( ZuulErrorFilter.class );

    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {
        //过滤顺序
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        //始终过滤
        return true;
    }

    @Override
    public Object run() {
        return null;
    }
}
