package cn.rock.filter;

import com.netflix.zuul.ZuulFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 路由之前
 *
 * @author rock
 * @date 2017/7/7
 */
@Component
public class ZuulPreFilter extends ZuulFilter {
    private static Logger logger = LoggerFactory.getLogger ( ZuulPreFilter.class );

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 6;
    }

    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public Object run() {
        return null;
    }
}
