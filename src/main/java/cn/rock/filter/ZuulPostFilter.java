package cn.rock.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.stereotype.Component;

/**
 * 路由之后
 *
 * @author rock
 * @date 2017/7/7
 */
@Component
public class ZuulPostFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 20;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext ();
        return ctx.getResponseStatusCode () >= 400;
    }

    @Override
    public Object run() {
        return null;
    }
}
